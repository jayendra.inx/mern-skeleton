Follow  this steps to start project :

1. setup mongodb connection url, mail settings in config.js

2. run "npm install" to install all the depedencies 

3. run "npm start" to start node server and react ui


Following features are avilable in this skeleton app :

- Redux, redux-persist
- cors at backend
- jwt authentication for all routes
- login/register/forgot password/reset password API
- api versoning