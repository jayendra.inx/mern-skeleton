var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var app = express();

// Loading routes
var api_V1 = require('./routes/v1/api');

// Adding cors
const cors = require('cors');
const config = require('./config');

// jwt middlewear
const jwt = require('./helpers/jwt');
const errorHandler = require('./helpers/error-handler');

// mongoose
const mongoose = require('mongoose');
const connectionURI = config.CONNECTION_URL;
const connectionOptions = { 
      useUnifiedTopology: true,
      useNewUrlParser: true
    };


// Check for mongodb connection 
mongoose.connect(connectionURI, connectionOptions,
	(err) => {
    	let result = {};
    	let status = 201;
    	if (!err) 
    	{
        	console.log('Mongodb connection establised successfully.');        
    	} else {
    		//console.log(err, 'actual error');
	      	console.log('Error while connecting to mongodb.'); 
    	}
	}
);


// Set ejs engine for node and static file path
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, '../client'));
app.use(express.static(path.join(__dirname, '../client')));


// Body parser middle wear
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));


// V1 API routes
app.use('/api/v1/', api_V1);
	

// Redirect all the routes to react component
app.get('*', (req, res) => {
	res.render('../client')
});

module.exports=app;
