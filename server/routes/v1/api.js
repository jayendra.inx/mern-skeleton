const express = require('express');
const Router  = express.Router();

// Auth Controller API
const auth_controller     = require('../../controllers/v1/auth.js');

Router.post('/auth/signup', auth_controller.signUp);
Router.post('/auth/login', auth_controller.login);
Router.post('/auth/forgotpwd', auth_controller.forgotpwd);
Router.post('/auth/resetpwd', auth_controller.resetpwd);

// another Controller API 

module.exports = Router;