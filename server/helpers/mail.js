const nodemailer = require('nodemailer');
const config = require('../config');

exports.transporter = nodemailer.createTransport({
    host: config.MAIL_HOST,
    port: config.MAIL_PORT,
    secure: true, 
    auth: {
        user: config.MAIL_ID,
    	pass: config.MAIL_PWD
    },
    tls: {
        rejectUnauthorized: false
    },
});
