const expressJwt = require('express-jwt');

module.exports = jwt;

function jwt() {
    let secret = {
	    "secret": "Inx2020"
	};

    return expressJwt({ secret, algorithms: ['HS256'] }).unless({
        path: [
            // public routes that don't require authentication

            // v1 Routes
            '/',
            '/api/v1/auth/signup',
            '/api/v1/auth/login',
            '/api/v1/auth/forgotpwd',
            '/api/v1/auth/resetpwd',
        ]
    });
}