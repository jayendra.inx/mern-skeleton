// Models loaded
const User = require('../../models/users');
const Resetpassword = require('../../models/resetpassword');

// jwt
const jsonwebtoken = require('jsonwebtoken');
const JWTSECRET = "Inx2020";

// For encrypt pwd
const bcrypt = require('bcrypt');

// Common config
const config = require('../../config');

// Setting mail
const transporter = require('../../helpers/mail').transporter;
const fromMail  = require('../../helpers/mail').FromMail;

//Common functions 
function haveErrors(req, res){
  if(!req.body.email || !req.body.password){
    return {status : 0, msg : "Provide email & password fields !!!"};
  }

  let foundMatch = USERS.filter(user => user.email === req.body.email);

  if(foundMatch.length){
    return {status : 0, msg : "User Already Exists! Login or choose another user email..."};
  }
  return {status : 1};
}


// Get list of avilable users
User.find(function(err, response){
  USERS = response;
}); 

//  sign-up api process
exports.signUp = function(req, res) {
	let haveError = haveErrors(req, res);

  	if(!haveError.status){
  		res.json(haveError);
  	} else {
  		var newUser = {name :req.body.name, email: req.body.email, password: req.body.password};
      	
      	let result = {};
  	    let status = 1;
  	    const { name, email, password } = req.body;
  	    const user = new User({ name, email, password }); 

        user.save((err, user) => {
	        if (!err) 
	        {
	          	// create a jwt token 
  			    const token  = jsonwebtoken.sign({ sub: user.email }, JWTSECRET, { expiresIn: '7d' });
  			    let data     = { user, token };
            	USERS.push(newUser);

  	        	res.json({status : 1, msg : "Registered user successfully...", data : data});
	        } else
	        {
            	console.log(err,'actual error');
	            res.send(err);
	        }
	    });
  	     
    }
}

// login api process
exports.login = function(req, res) {
	if(!req.body.email || !req.body.password){
      return {status : 0, msg : "Provide email & password fields !!!"};
   	} else {
    	const { email, password } = req.body;

    	let result = {};
    	let status = 1;
      
    	User.findOne({email}, (err, user) => {
          if (!err && user) {
            bcrypt.compare(password, user.password).then(match => {
              if (match) {
                status = 1;
                // Create a token
                const payload = { user: user.email };
                const options = { expiresIn: '7d', issuer: 'https://google.com' };
                const token = jsonwebtoken.sign(payload, JWTSECRET, options);

                // console.log('TOKEN', token);
                status = 1;                
                result.token = token;                
                result.result = user;
                console.log(result);
              } else {
              	status = 0;
              }

              if(status){
            	res.json({status : 1, message: 'successfully login...', data : result});
	            } else {
	            	res.json({status : 0, message: 'Authentication error'});
	            }
            });
            
          } else {
            res.json({status : 0, message: 'invalid useremail or password'});
          }
    	});      
   }
}

// forgot password api process
exports.forgotpwd = function(req, res) {
	if(!req.body.email) {
      res.json({status : 0, message: "Provide email field !!!"});
   	} else 
   	{
    	let foundMatch = USERS.filter(user => user.email == req.body.email);

      	if(foundMatch.length)
      	{
			var current = new Date ();
			var expiretime = new Date ( current );
			expiretime.setMinutes(current.getMinutes() + 30);
			expiretime = Math.round(expiretime.getTime()/1000);

			var email = req.body.email;

			let resettoken = Math.random().toString(36).slice(2) +  
							Math.random().toString(36).toUpperCase().slice(2);

			var mailOptions = {
				from: config.MAIL_FROM,
				to: email,
				subject: 'Reset password',
				html: "To reset password <a href='http://localhost:3333/resetpwd?reset="+resettoken+"'>click here</a>"
			};

      		transporter.sendMail(mailOptions, function(error, info){
      		  	if (error) {
      		    	res.json({status : 0, message: 'mail error...', errorlog : error});
      		  	} else {
             		let result = {};
              		let status = 1;
                
              		const resetmodel = new Resetpassword({ resettoken, expiretime, email }); 
                  
              		resetmodel.save((err, user) => {
    	                if(!err){
    	                    res.json({status : 1, message: 'Reset password link sent to your email...'});
    	                } else {
    	                    res.json({status : 0, msg : err});
    	                }
    		        });
      		  	}
      		});
    	} else {
    		res.json({status : 0, message: "Invalid email!"});
    	}
   	}
}

// Reset password api process
exports.resetpwd = function(req, res) {
	let resetToken = req.query.reset;

	if(!req.body.password || !req.body.cpassword){
    	res.json({status : 0, message: "Provide password and cpassword field !!!"});
  	} else if(req.body.password != req.body.cpassword){
    	res.json({status : 0, message: "password and cpassword must match !!!"});
  	}
  	else if(resetToken)
  	{
      	let result = {};
      	let status = 1;
      	Resetpassword.findOne({resettoken: resetToken}, (err, data) => {
          	if(err) throw res.json({status : 0, error : err});;

          	var current = new Date ();
          	var expiretime = data.expiretime;
          	current = Math.round(current.getTime()/1000);

          	if(current <= expiretime){
            	var pwd = req.body.password;
            	var email = data.email;

	            bcrypt.hash(pwd, 10, function(err, hash) {
	              if (err) {
	                res.json({status : 0, msg : "there is some error try again..."});
	              } else {

	                User.findOneAndUpdate({email: email}, {password : hash}, (err, data) => {
	                  res.json({status : 1, msg : "password reset successfully..."});
	                });
	              }
	            });            
          	} else {
            	res.json({status : 0, msg : "reset link has been expired"});
          	}
      	});
  	} else {
    	res.json({status : 0, message: "Invalid url !!!"});
  	}
}