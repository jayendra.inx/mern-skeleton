const mongoose = require('mongoose');

// schema maps to a collection
const Schema = mongoose.Schema;

const resetpasswordSchema = new Schema({
  resettoken: String,
  expiretime:Number,
  email:String,
});


module.exports = mongoose.model('Resetpassword', resetpasswordSchema);