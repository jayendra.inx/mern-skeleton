import {
  CHANGE_DATA,
} from './types';

export const changeData = () => dispatch => {
  dispatch({
    type: CHANGE_DATA
  });
};

